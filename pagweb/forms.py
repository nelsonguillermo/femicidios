from django import forms
from .models import *


class OcupacionForm(forms.ModelForm):
    class Meta:
        model = Ocupacion
        fields = ['id_ocupacion', 'nombre_ocupacion']


class PersonaForm(forms.ModelForm):
    class Meta:
        model = Persona
        fields = ['id_persona', 'nombre', 'apellidos', 'edad', 'id_ocupacion']


class PaisForm(forms.ModelForm):
    class Meta:
        model = Pais
        fields = ['idPais', 'nombre']


class DepartamentoForm(forms.ModelForm):
    class Meta:
        model = Departamento
        fields = ['idDepartamento', 'nombre', 'idPais']


class ProvinciaForm(forms.ModelForm):
    class Meta:
        model = Provincia
        fields = ['idProvincia', 'nombre', 'idDepartamento']


class LugarForm(forms.ModelForm):
    class Meta:
        model = Lugar
        fields = ['idLugar', 'nombre', 'idProvincia']


class TipoArmaForm(forms.ModelForm):
    class Meta:
        model = TipoArma
        fields = ['id_tipo_arma', 'nombre_tipo_arma']


class InformeForm(forms.ModelForm):
    class Meta:
        model = Informe
        fields = ['idInforme', 'estado', 'situacion', 'sentencia']


class RelacionVictimaForm(forms.ModelForm):
    class Meta:
        model = RelacionVictima
        fields = ['idRelacion', 'tipoRelacion', 'agresionPrevia', 'tiempoRelacion']


class UrlForm(forms.ModelForm):
    class Meta:
        model = Url
        fields = ['idUrl', 'nombreUrl', 'fecha', 'texto', 'palabra_clave', 'categoria', 'autor']


class FemicidioForm(forms.ModelForm):
    class Meta:
        model = Femicidio
        fields = ['idFemicidio', 'fecha', 'causa', 'idLugar', 'idInforme', 'idRelacion', 'id_tipo_arma', 'url']


class TipoPersonaForm(forms.ModelForm):
    class Meta:
        model = TipoPersona
        fields = ['id_tipo_persona', 'nombre_tipo']


class FemicidioPersonaForm(forms.ModelForm):
    class Meta:
        model = FemicidioPersona
        fields = ['id_femicidio_persona', 'idFemicidio', 'id_persona', 'id_tipo_persona']
