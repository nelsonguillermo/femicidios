from django.urls import reverse
from django.conf import settings
from django.db import models
from django.db.models import *

# Create your models here.
class Ocupacion(models.Model):
    id_ocupacion = models.AutoField(primary_key=True)
    nombre_ocupacion = models.CharField(max_length=75)

    class Meta:
        db_table = 'ocupacion'

    def __unicode__(self):
        return u'%s' % self.nombre_ocupacion

    def get_absolute_url(self):
        return reverse('ocupacion_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('ocupacion_update', args=(self.pk,))


class Persona(models.Model):
    id_persona = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    edad = models.IntegerField()
    id_ocupacion = models.ForeignKey(Ocupacion, models.DO_NOTHING, db_column='id_ocupacion' )

    class Meta:
        db_table = 'persona'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('persona_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('persona_update', args=(self.pk,))

class Pais(models.Model):
    idPais = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)

    class Meta:
        db_table = 'pais'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('pais_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('pais_update', args=(self.pk,))


class Departamento(models.Model):
    idDepartamento = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    idPais = models.ForeignKey(Pais, models.DO_NOTHING, db_column='idPais' )

    class Meta:
        db_table = 'departamento'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('departamento_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('departamento_update', args=(self.pk,))


class Provincia(models.Model):
    idProvincia = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    idDepartamento = models.ForeignKey(Departamento, models.DO_NOTHING, db_column='idDepartamento' )

    class Meta:
        db_table = 'provincia'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('provincia_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('provincia_update', args=(self.pk,))


class Lugar(models.Model):
    idLugar = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    idProvincia = models.ForeignKey(Provincia, models.DO_NOTHING, db_column='idProvincia' )

    class Meta:
        db_table = 'lugar'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('lugar_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('lugar_update', args=(self.pk,))


class TipoArma(models.Model):
    id_tipo_arma = models.AutoField(primary_key=True)
    nombre_tipo_arma = models.CharField(max_length=100)

    class Meta:
        db_table = 'tipo_arma'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('tipoarma_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('tipoarma_update', args=(self.pk,))


class Informe(models.Model):
    idInforme = models.AutoField(primary_key=True)
    estado = models.CharField(max_length=150)
    situacion = models.CharField(max_length=150)
    sentencia = models.CharField(max_length=150)

    class Meta:
        db_table = 'informe'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('informe_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('informe_update', args=(self.pk,))


class RelacionVictima(models.Model):
    idRelacion = models.AutoField(primary_key=True)
    tipoRelacion = models.CharField(max_length=150)
    agresionPrevia = models.CharField(max_length=150)
    tiempoRelacion = models.CharField(max_length=150)

    class Meta:
        db_table = 'relacion_victima'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('relacionvictima_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('relacionvictima_update', args=(self.pk,))


class Url(models.Model):
    idUrl = models.AutoField(primary_key=True)
    nombreUrl = models.CharField(max_length=150)
    fecha = models.DateField()
    texto = models.CharField(max_length=5000)
    palabra_clave = models.CharField(max_length=100)
    categoria = models.CharField(max_length=100)
    autor = models.CharField(max_length=150)

    class Meta:
        db_table = 'url'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('url_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('url_update', args=(self.pk,))


class Femicidio(models.Model):
    idFemicidio = models.AutoField(primary_key=True)
    fecha = models.DateField()
    causa = models.CharField(max_length=150)
    idLugar = models.ForeignKey(Lugar, models.DO_NOTHING, db_column='idLugar' )
    idInforme = models.ForeignKey(Informe, models.DO_NOTHING, db_column='idInforme' )
    idRelacion = models.ForeignKey(RelacionVictima, models.DO_NOTHING, db_column='idRelacion' )
    id_tipo_arma = models.ForeignKey(TipoArma, models.DO_NOTHING, db_column='id_tipo_arma' )
    url = models.ManyToManyField(Url)

    class Meta:
        db_table = 'femicidio'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('femicidio_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('femicidio_update', args=(self.pk,))


class TipoPersona(models.Model):
    id_tipo_persona = models.AutoField(primary_key=True)
    nombre_tipo = models.CharField(max_length=150)

    class Meta:
        db_table = 'tipo_persona'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('tipopersona_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('tipopersona_update', args=(self.pk,))


class FemicidioPersona(models.Model):
    id_femicidio_persona = models.AutoField(primary_key=True)
    idFemicidio = models.ForeignKey(Femicidio, models.DO_NOTHING, db_column='idFemicidio' )
    id_persona = models.ForeignKey(Persona, models.DO_NOTHING, db_column='id_persona' )
    id_tipo_persona = models.ForeignKey(TipoPersona, models.DO_NOTHING, db_column='id_tipo_persona' )

    class Meta:
        db_table = 'femicidio_persona'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('femicidiopersona_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('femicidiopersona_update', args=(self.pk,))
