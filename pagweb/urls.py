from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path('', views.index, name='index'),
    path('ocupacion/', views.OcupacionListView.as_view(), name='ocupacion_list'),
    path('ocupacion/create/', views.OcupacionCreateView.as_view(), name='ocupacion_create'),
    path('ocupacion/detail/<int:pk>/', views.OcupacionDetailView.as_view(), name='ocupacion_detail'),
    path('ocupacion/update/<int:pk>/', views.OcupacionUpdateView.as_view(), name='ocupacion_update'),

    path('persona/', views.PersonaListView.as_view(), name='persona_list'),
    path('persona/create/', views.PersonaCreateView.as_view(), name='persona_create'),
    path('persona/detail/<int:pk>/', views.PersonaDetailView.as_view(), name='persona_detail'),
    path('persona/update/<int:pk>/', views.PersonaUpdateView.as_view(), name='persona_update'),
    # urls for Pais
    path('pais/', views.PaisListView.as_view(), name='pais_list'),
    path('pais/create/', views.PaisCreateView.as_view(), name='pais_create'),
    path('pais/detail/<int:pk>/', views.PaisDetailView.as_view(), name='pais_detail'),
    path('pais/update/<int:pk>/', views.PaisUpdateView.as_view(), name='pais_update'),
    # urls for Departamento
    path('departamento/', views.DepartamentoListView.as_view(), name='departamento_list'),
    path('departamento/create/', views.DepartamentoCreateView.as_view(), name='departamento_create'),
    path('departamento/detail/<int:pk>/', views.DepartamentoDetailView.as_view(), name='departamento_detail'),
    path('departamento/update/<int:pk>/', views.DepartamentoUpdateView.as_view(), name='departamento_update'),
    # urls for Provincia
    path('provincia/', views.ProvinciaListView.as_view(), name='provincia_list'),
    path('provincia/create/', views.ProvinciaCreateView.as_view(), name='provincia_create'),
    path('provincia/detail/<int:pk>/', views.ProvinciaDetailView.as_view(), name='provincia_detail'),
    path('provincia/update/<int:pk>/', views.ProvinciaUpdateView.as_view(), name='provincia_update'),
    # urls for Lugar
    path('lugar/', views.LugarListView.as_view(), name='lugar_list'),
    path('lugar/create/', views.LugarCreateView.as_view(), name='lugar_create'),
    path('lugar/detail/<int:pk>/', views.LugarDetailView.as_view(), name='lugar_detail'),
    path('lugar/update/<int:pk>/', views.LugarUpdateView.as_view(), name='lugar_update'),
    # urls for TipoArma
    path('tipoarma/', views.TipoArmaListView.as_view(), name='tipoarma_list'),
    path('tipoarma/create/', views.TipoArmaCreateView.as_view(), name='tipoarma_create'),
    path('tipoarma/detail/<int:pk>/', views.TipoArmaDetailView.as_view(), name='tipoarma_detail'),
    path('tipoarma/update/<int:pk>/', views.TipoArmaUpdateView.as_view(), name='tipoarma_update'),

    path('informe/', views.InformeListView.as_view(), name='informe_list'),
    path('informe/create/', views.InformeCreateView.as_view(), name='informe_create'),
    path('informe/detail/<int:pk>/', views.InformeDetailView.as_view(), name='informe_detail'),
    path('informe/update/<int:pk>/', views.InformeUpdateView.as_view(), name='informe_update'),
    # urls for RelacionVictima
    path('relacionvictima/', views.RelacionVictimaListView.as_view(), name='relacionvictima_list'),
    path('relacionvictima/create/', views.RelacionVictimaCreateView.as_view(), name='relacionvictima_create'),
    path('relacionvictima/detail/<int:pk>/', views.RelacionVictimaDetailView.as_view(), name='relacionvictima_detail'),
    path('relacionvictima/update/<int:pk>/', views.RelacionVictimaUpdateView.as_view(), name='relacionvictima_update'),
    # urls for Url
    path('url/', views.UrlListView.as_view(), name='url_list'),
    path('url/create/', views.UrlCreateView.as_view(), name='url_create'),
    path('url/detail/<int:pk>/', views.UrlDetailView.as_view(), name='url_detail'),
    path('url/update/<int:pk>/', views.UrlUpdateView.as_view(), name='url_update'),
    # urls for Femicidio
    path('femicidio/', views.FemicidioListView.as_view(), name='femicidio_list'),
    path('femicidio/create/', views.FemicidioCreateView.as_view(), name='femicidio_create'),
    path('femicidio/detail/<int:pk>/', views.FemicidioDetailView.as_view(), name='femicidio_detail'),
    path('femicidio/update/<int:pk>/', views.FemicidioUpdateView.as_view(), name='femicidio_update'),
    # urls for TipoPersona
    path('tipopersona/', views.TipoPersonaListView.as_view(), name='tipopersona_list'),
    path('tipopersona/create/', views.TipoPersonaCreateView.as_view(), name='tipopersona_create'),
    path('tipopersona/detail/<int:pk>/', views.TipoPersonaDetailView.as_view(), name='tipopersona_detail'),
    path('tipopersona/update/<int:pk>/', views.TipoPersonaUpdateView.as_view(), name='tipopersona_update'),
    # urls for FemicidioPersona
    path('femicidiopersona/', views.FemicidioPersonaListView.as_view(), name='femicidiopersona_list'),
    path('femicidiopersona/create/', views.FemicidioPersonaCreateView.as_view(), name='femicidiopersona_create'),
    path('femicidiopersona/detail/<int:pk>/', views.FemicidioPersonaDetailView.as_view(), name='femicidiopersona_detail'),
    path('femicidiopersona/update/<int:pk>/', views.FemicidioPersonaUpdateView.as_view(), name='femicidiopersona_update'),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
