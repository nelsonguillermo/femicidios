from django.shortcuts import render
from django.views.generic import DetailView, ListView, UpdateView, CreateView
from .models import *
from .forms import *

# Create your views here.
def index(request):
    ctx = {}
    return render(request, 'base.html',ctx)

class OcupacionListView(ListView):
    model = Ocupacion


class OcupacionCreateView(CreateView):
    model = Ocupacion
    form_class = OcupacionForm


class OcupacionDetailView(DetailView):
    model = Ocupacion


class OcupacionUpdateView(UpdateView):
    model = Ocupacion
    form_class = OcupacionForm


class PersonaListView(ListView):
    model = Persona


class PersonaCreateView(CreateView):
    model = Persona
    form_class = PersonaForm


class PersonaDetailView(DetailView):
    model = Persona


class PersonaUpdateView(UpdateView):
    model = Persona
    form_class = PersonaForm


class PaisListView(ListView):
    model = Pais


class PaisCreateView(CreateView):
    model = Pais
    form_class = PaisForm


class PaisDetailView(DetailView):
    model = Pais


class PaisUpdateView(UpdateView):
    model = Pais
    form_class = PaisForm


class DepartamentoListView(ListView):
    model = Departamento


class DepartamentoCreateView(CreateView):
    model = Departamento
    form_class = DepartamentoForm


class DepartamentoDetailView(DetailView):
    model = Departamento


class DepartamentoUpdateView(UpdateView):
    model = Departamento
    form_class = DepartamentoForm


class ProvinciaListView(ListView):
    model = Provincia


class ProvinciaCreateView(CreateView):
    model = Provincia
    form_class = ProvinciaForm


class ProvinciaDetailView(DetailView):
    model = Provincia


class ProvinciaUpdateView(UpdateView):
    model = Provincia
    form_class = ProvinciaForm


class LugarListView(ListView):
    model = Lugar


class LugarCreateView(CreateView):
    model = Lugar
    form_class = LugarForm


class LugarDetailView(DetailView):
    model = Lugar


class LugarUpdateView(UpdateView):
    model = Lugar
    form_class = LugarForm


class TipoArmaListView(ListView):
    model = TipoArma


class TipoArmaCreateView(CreateView):
    model = TipoArma
    form_class = TipoArmaForm


class TipoArmaDetailView(DetailView):
    model = TipoArma


class TipoArmaUpdateView(UpdateView):
    model = TipoArma
    form_class = TipoArmaForm


class InformeListView(ListView):
    model = Informe


class InformeCreateView(CreateView):
    model = Informe
    form_class = InformeForm


class InformeDetailView(DetailView):
    model = Informe


class InformeUpdateView(UpdateView):
    model = Informe
    form_class = InformeForm


class RelacionVictimaListView(ListView):
    model = RelacionVictima


class RelacionVictimaCreateView(CreateView):
    model = RelacionVictima
    form_class = RelacionVictimaForm


class RelacionVictimaDetailView(DetailView):
    model = RelacionVictima


class RelacionVictimaUpdateView(UpdateView):
    model = RelacionVictima
    form_class = RelacionVictimaForm


class UrlListView(ListView):
    model = Url


class UrlCreateView(CreateView):
    model = Url
    form_class = UrlForm


class UrlDetailView(DetailView):
    model = Url


class UrlUpdateView(UpdateView):
    model = Url
    form_class = UrlForm


class FemicidioListView(ListView):
    model = Femicidio


class FemicidioCreateView(CreateView):
    model = Femicidio
    form_class = FemicidioForm


class FemicidioDetailView(DetailView):
    model = Femicidio


class FemicidioUpdateView(UpdateView):
    model = Femicidio
    form_class = FemicidioForm


class TipoPersonaListView(ListView):
    model = TipoPersona


class TipoPersonaCreateView(CreateView):
    model = TipoPersona
    form_class = TipoPersonaForm


class TipoPersonaDetailView(DetailView):
    model = TipoPersona


class TipoPersonaUpdateView(UpdateView):
    model = TipoPersona
    form_class = TipoPersonaForm


class FemicidioPersonaListView(ListView):
    model = FemicidioPersona


class FemicidioPersonaCreateView(CreateView):
    model = FemicidioPersona
    form_class = FemicidioPersonaForm


class FemicidioPersonaDetailView(DetailView):
    model = FemicidioPersona


class FemicidioPersonaUpdateView(UpdateView):
    model = FemicidioPersona
    form_class = FemicidioPersonaForm
